import useTranslation from "next-translate/useTranslation";

import { useRouter } from "next/dist/client/router";
import React from "react";
const Blogs = () => {
  const router = useRouter();
  const { t, lang } = useTranslation();

  return <div>lang {lang}</div>;
};

export default Blogs;
